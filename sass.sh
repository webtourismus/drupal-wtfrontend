#!/usr/bin/env sh

sass ./scss/libraries/sal.scss ./css/sal.css --style expanded
sass ./scss/libraries/glide.scss ./css/glide.css --style expanded
