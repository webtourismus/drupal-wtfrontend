/**
 * @file
 * Webtourismus Frontend behaviors.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';
  drupalSettings.wt = drupalSettings.wt || {};
  drupalSettings.wt.sal = drupalSettings.wt.sal || {};
  drupalSettings.wt.sal.options = drupalSettings.wt.sal.options || {};
  drupalSettings.wt.sal.options.threshold = drupalSettings.wt.sal.options.threshold ?? 0.25;
  drupalSettings.wt.sal.options.once = drupalSettings.wt.sal.options.once ?? true;

  Drupal.behaviors.wtSal = Drupal.behaviors.wtSal || {
    attach: function (context, settings) {
      $(document, context).once('wtSal').each( function() {
        drupalSettings.wt.sal.sal = sal(drupalSettings.wt.sal.options);
        if (drupalSettings.wt.isAdminTheme ?? false) {
          drupalSettings.wt.sal.sal.disable();
        }
      });
    }
  }
} (jQuery, Drupal, drupalSettings));
