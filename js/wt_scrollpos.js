(function ($, Drupal, drupalSettings) {
  'use strict';
  drupalSettings.wt = drupalSettings.wt || {};
  drupalSettings.wt.scrollpos = drupalSettings.wt.scrollpos || {};
  drupalSettings.wt.scrollpos.options = drupalSettings.wt.scrollpos.options || {};
  drupalSettings.wt.scrollpos.options.scrollOffsetY = (function(){
    return document.querySelector('.wt__header').classList.contains('wt__header--banner') ? document.querySelector('.wt__header').offsetHeight : 0
  })();


  Drupal.behaviors.wtScrollpos = Drupal.behaviors.wtScrollpos || {
    attach: function (context, settings) {
      $(document, context).once('wtScrollpos').each( function() {
        ScrollPosStyler.init(drupalSettings.wt.scrollpos.options);
      });
    }
  }

}(jQuery, Drupal, drupalSettings));
