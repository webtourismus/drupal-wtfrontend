(function ($, Drupal, drupalSettings) {
  'use strict';
  drupalSettings.wt = drupalSettings.wt || {};
  drupalSettings.wt.glide = drupalSettings.wt.glide || {};
  drupalSettings.wt.glide.glide = drupalSettings.wt.glide.glide || [];
  drupalSettings.wt.glide.options = drupalSettings.wt.glide.options || {};
  drupalSettings.wt.glide.options.type = drupalSettings.wt.glide.options.type || 'carousel';
  drupalSettings.wt.glide.options.focusAt = drupalSettings.wt.glide.options.focusAt ?? 'center';
  drupalSettings.wt.glide.options.autoplay = drupalSettings.wt.isAdminTheme ? false : (drupalSettings.wt.glide.options.autoplay ?? false);
  drupalSettings.wt.glide.options.dragThreshold = drupalSettings.wt.isAdminTheme ? false : (drupalSettings.wt.glide.options.dragThreshold ?? 120);
  drupalSettings.wt.glide.options.swipeThreshold = drupalSettings.wt.isAdminTheme ? false : (drupalSettings.wt.glide.options.swipeThreshold ?? 80);


  /**
   * Create carousels with glide JS
   */
  Drupal.behaviors.wtGlide = Drupal.behaviors.wtGlide || {
    attach: function (context, settings) {
      $('.glide', context).once('wtGlide').each( function() {
        drupalSettings.wt.glide.glide.push( new Glide(this, drupalSettings.wt.glide.options).mount() );
      });
    }
  }
} (jQuery, Drupal, drupalSettings));
