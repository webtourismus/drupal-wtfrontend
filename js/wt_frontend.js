(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.wt = Drupal.wt || {};
  drupalSettings.wt = drupalSettings.wt || {};

  /**
   * Move local tasks into toolbar
   */
  Drupal.behaviors.wtLocalTasks = Drupal.behaviors.wtLocalTasks || {
    attach: function (context, settings) {
      $(document, context).once('wtLocalTasks').each(function () {
        let toolbar = document.getElementById('toolbar-bar');
        if (toolbar) {
          let editLink = document.querySelector('#block-localtask a[data-drupal-link-system-path^="node/"][data-drupal-link-system-path$="/edit"], #block-localtask a[data-drupal-link-system-path="admin/lunch/week"]');
          if (editLink) {
            toolbar.insertAdjacentHTML('beforeend', '<div class="toolbar-tab"><a href="' + editLink.getAttribute('href') + '" class="toolbar-icon toolbar-icon-tablink-edit toolbar-icon-tablink toolbar-item"><i class="fas fa-edit"></i>' + editLink.innerHTML + '</a></div>');
          }
          let layoutLink = document.querySelector('#block-localtask a[data-drupal-link-system-path^="node/"][data-drupal-link-system-path$="/layout"]');
          if (layoutLink) {
            toolbar.insertAdjacentHTML('beforeend', '<div class="toolbar-tab"><a href="' + layoutLink.getAttribute('href') + '" class="toolbar-icon toolbar-item"><i class="fas fa-th-large"></i>' + layoutLink.innerHTML + '</a></div>');
          }
          let translateLink = document.querySelector('#block-localtask a[data-drupal-link-system-path^="node/"][data-drupal-link-system-path$="/translations"]');
          if (translateLink) {
            toolbar.insertAdjacentHTML('beforeend', '<div class="toolbar-tab"><a href="' + translateLink.getAttribute('href') + '" class="toolbar-icon toolbar-item"><i class="fas fa-language"></i>' + translateLink.innerHTML + '</a></div>');
          }
          let previewLink = document.querySelector('.node-preview-container a.node-preview-backlink');
          if (previewLink) {
            toolbar.insertAdjacentHTML('beforeend', '<div class="toolbar-tab"><a href="' + previewLink.getAttribute('href') + '" class="toolbar-icon toolbar-item"><i class="fas fa-arrow-alt-left"></i>' + previewLink.innerHTML + '</a></div>');
          }
        }
      });
    }
  };

  Drupal.wt.setViewportHeightProperty = Drupal.wt.setViewportHeightProperty || function () {
    let realVh = window.innerHeight + 'px';
    document.documentElement.style.setProperty('--viewport-height', realVh);
  }

  Drupal.behaviors.wtSetViewportHeight = Drupal.behaviors.wtSetViewportHeight || {
    attach: function (context, settings) {
      $(document, context).once('wtSetViewportHeight').each(function () {
        Drupal.wt.setViewportHeightProperty();
        window.addEventListener('orientationchange', Drupal.wt.setViewportHeightProperty);
        window.addEventListener('scroll', Drupal.wt.setViewportHeightProperty);
        window.addEventListener('resize', Drupal.wt.setViewportHeightProperty);
      });
    }
  }

  Drupal.wt.getFixedTop = Drupal.wt.getFixedTop || function () {
    return getComputedStyle(document.querySelector('body')).getPropertyValue('--fixed-top');
  }

  Drupal.wt.toggle = Drupal.wt.toggle || {};
  Drupal.wt.toggle.options = Drupal.wt.toggle.options || {};
  Drupal.wt.toggle.options.duration = Drupal.wt.toggle.options.duration ?? 400;

  /**
   * Simple visibility toggle that only switches classes from
   * .is-collapsed => .is-unstable.is-collapsed => .is-expanded =>
   * .is-unstable.is-expanded => .is-collapsed for the element to be animated
   * corresponding CSS styles are required in the SASS files
   */
  Drupal.wt.toggle.toggle = Drupal.wt.toggle.toggle || function (element) {
    let source = element;
    let target = document.querySelector(source.getAttribute('data-target'));

    if (!target.classList.contains('is-unstable')) {
      target.classList.add('is-unstable');

      let options = Drupal.wt.toggle.options;
      if (source.getAttribute('data-animation-duration') == parseInt(source.getAttribute('data-animation-duration'), 10)) {
        options.duration = source.getAttribute('data-animation-duration');
      }

      if (target.classList.contains('is-expanded')) {
        window.setTimeout(function () {
          source.setAttribute('aria-expanded', 'false');
          target.classList.remove('is-unstable');
          target.classList.remove('is-expanded');
          target.classList.add('is-collapsed');
        }, options.duration);
      }
      else {
        window.setTimeout(function () {
          source.setAttribute('aria-expanded', 'true');
          target.classList.remove('is-unstable');
          target.classList.remove('is-collapsed');
          target.classList.add('is-expanded');
        }, options.duration);
      }
    }
  }


  /**
   * Toogle visibility using jQuery's fadeIn/fadeOut functions. Useful to
   * animate to "height: auto" which cannot be done with CSS
   */
  Drupal.wt.toggle.fade = Drupal.wt.toggle.fade || function (element) {
    let source = element;
    let target = document.querySelector(source.getAttribute('data-target'));

    if (!target.classList.contains('is-unstable')) {
      target.classList.add('is-unstable');

      let options = Drupal.wt.toggle.options;
      if (source.getAttribute('data-animation-duration') == parseInt(source.getAttribute('data-animation-duration'), 10)) {
        options.duration = source.getAttribute('data-animation-duration');
      }

      let $target = $(target);
      if (target.classList.contains('is-expanded')) {
        options.complete = function () {
          source.setAttribute('aria-expanded', 'false');
          target.classList.remove('is-unstable');
          target.classList.remove('is-expanded');
          target.classList.add('is-collapsed');
        }
        $target['fadeOut'](options);
      }
      else {
        if (source.getAttribute('data-toggle-to-display')) {
          //by default jQuery would animate to "display: block", the following
          //line will tells jQuery to animate to some other CSS display setting
          $target.css('display', source.getAttribute('data-toggle-to-display')).hide();
          source.removeAttribute('data-toggle-to-display');
        }
        options.complete = function () {
          source.setAttribute('aria-expanded', 'true');
          target.classList.remove('is-unstable');
          target.classList.remove('is-collapsed');
          target.classList.add('is-expanded');
        }
        $(target)['fadeIn'](options);
      }
    }
  }

  /**
   * Toogle visibility using jQuery's slideDown/slideUp functions. Useful to
   * animate to "height: auto" which cannot be done with CSS
   */
  Drupal.wt.toggle.collapse = Drupal.wt.toggle.collapse || function (element) {
    let source = element;
    let target = document.querySelector(source.getAttribute('data-target'));

    if (!target.classList.contains('is-unstable')) {
      target.classList.add('is-unstable');

      let options = Drupal.wt.toggle.options;
      if (source.getAttribute('data-animation-duration') == parseInt(source.getAttribute('data-animation-duration'), 10)) {
        options.duration = source.getAttribute('data-animation-duration');
      }

      let $target = $(target);
      if (target.classList.contains('is-expanded')) {
        options.complete = function () {
          source.setAttribute('aria-expanded', 'false');
          target.classList.remove('is-unstable');
          target.classList.remove('is-expanded');
          target.classList.add('is-collapsed');
        }
        $target['slideUp'](options);
      }
      else {
        if (source.getAttribute('data-toggle-to-display')) {
          //by default jQuery would animate to "display: block", the following
          //line will tells jQuery to animate to some other CSS display setting
          $target.css('display', source.getAttribute('data-toggle-to-display')).hide();
          source.removeAttribute('data-toggle-to-display');
        }
        options.complete = function () {
          source.setAttribute('aria-expanded', 'true');
          target.classList.remove('is-unstable');
          target.classList.remove('is-collapsed');
          target.classList.add('is-expanded');
        }
        $(target)['slideDown'](options);
      }
    }
  }

  /**
   * Toogle visibility using jQuery's animate function. Useful to animate
   * to "width: auto" which cannot be done with CSS
   */
  Drupal.wt.toggle.slide = Drupal.wt.toggle.slide || function (element) {
    let source = element;
    let target = document.querySelector(source.getAttribute('data-target'));

    if (!target.classList.contains('is-unstable')) {
      target.classList.add('is-unstable');

      let options = Drupal.wt.toggle.options;
      if (source.getAttribute('data-animation-duration') == parseInt(source.getAttribute('data-animation-duration'), 10)) {
        options.duration = source.getAttribute('data-animation-duration');
      }

      let $target = $(target);
      if (target.classList.contains('is-expanded')) {
        options.complete = function () {
          source.setAttribute('aria-expanded', 'false');
          target.classList.remove('is-unstable');
          target.classList.remove('is-expanded');
          target.classList.add('is-collapsed');
        }
        $target['animate']({
          'width': 'toggle',
          'padding': 'toggle',
          'border': 'toggle',
          'margin': 'toggle'
        }, options);
      }
      else {
        if (source.getAttribute('data-toggle-to-display')) {
          //by default jQuery would animate to "display: block", the following
          //line will tells jQuery to animate to some other CSS display setting
          $target.css('display', source.getAttribute('data-toggle-to-display')).hide();
          source.removeAttribute('data-toggle-to-display');
        }
        options.complete = function () {
          source.setAttribute('aria-expanded', 'true');
          target.classList.remove('is-unstable');
          target.classList.remove('is-collapsed');
          target.classList.add('is-expanded');
        }
        $target['animate']({
          'width': 'toggle',
          'padding': 'toggle',
          'border': 'toggle',
          'margin': 'toggle'
        }, options);
      }
    }
  }

  Drupal.behaviors.wtToggleVisibility = Drupal.behaviors.wtToggleVisibility || {
    attach: function (context, settings) {
      $('[data-toggle]', context).once('wtToggleVisibility').each(function () {
        $(this).on('click', function (event) {
          let toggleFunction = event.currentTarget.getAttribute('data-toggle');
          Drupal.wt.toggle[toggleFunction](event.currentTarget);
          event.preventDefault();
          event.stopPropagation();
        });
      });
    }
  }

  Drupal.wt.menu = Drupal.wt.menu || {};

  Drupal.wt.menu.fade = Drupal.wt.menu.fade || function (ul) {
    if (!ul.classList.contains('is-unstable')) {
      ul.classList.add('is-unstable');
      let display = getComputedStyle(ul).getPropertyValue('display') || 'none';
      let $ul = $(ul);
      if (display == 'none') {
        $ul.stop(true, true).fadeIn({
          'complete': function () {
            ul.classList.remove('is-collapsed');
            ul.classList.remove('is-unstable');
            ul.classList.add('is-expanded');
          }
        });
      }
      else {
        $ul.stop(true, true).fadeOut({
          'complete': function () {
            ul.classList.remove('is-expanded');
            ul.classList.remove('is-unstable');
            ul.classList.add('is-collapsed');
          }
        });
      }
    }
  }

  Drupal.wt.menu.collapse = Drupal.wt.menu.collapse || function (ul) {
    if (!ul.classList.contains('is-unstable')) {
      ul.classList.add('is-unstable');
      let display = getComputedStyle(ul).getPropertyValue('display') || 'none';
      let $ul = $(ul);
      if (display === 'none') {
        $ul.stop(true, true).slideDown({
          'complete': function () {
            ul.classList.remove('is-collapsed');
            ul.classList.remove('is-unstable');
            ul.classList.add('is-expanded');
          }
        });
      }
      else {
        $ul.stop(true, true).slideUp({
          'complete': function () {
            ul.classList.remove('is-expanded');
            ul.classList.remove('is-unstable');
            ul.classList.add('is-collapsed');
          }
        });
      }
    }
  }

  Drupal.wt.menu.slide = Drupal.wt.menu.slide || function (ul) {
    if (!ul.classList.contains('is-unstable')) {
      ul.classList.add('is-unstable');
      let display = getComputedStyle(ul).getPropertyValue('display') || 'none';
      let $ul = $(ul);
      if (display === 'none') {
        $ul.stop(true, true).animate(
          {
            'width': 'toggle',
            'padding': 'toggle',
            'margin': 'toggle',
            'border': 'toggle',
          },
          {
            'complete': function () {
              ul.classList.remove('is-collapsed');
              ul.classList.remove('is-unstable');
              ul.classList.add('is-expanded');
            }
          }
        );
      }
      else {
        $ul.stop(true, true).animate(
          {
            'width': 'toggle',
            'padding': 'toggle',
            'margin': 'toggle',
            'border': 'toggle',
          },
          {
            'complete': function () {
              ul.classList.remove('is-expanded');
              ul.classList.remove('is-unstable');
              ul.classList.add('is-collapsed');
            }
          }
        );
      }
    }
  }

  Drupal.behaviors.wtMenuToggle = Drupal.behaviors.wtMenuToggle || {
    attach: function (context, settings) {
      $('.nav__a[href$="#toggle"]', context).once('wtMenuToggle').each(function () {
        $(this).on('click', function (event) {
          let ul = this.nextElementSibling;
          if (ul && ul.tagName == 'UL') {
            let display = getComputedStyle(ul).getPropertyValue('display') || 'none';
            let toggleFunction = (getComputedStyle(ul).getPropertyValue('--menu--animation') || 'collapse').trim();
            if (display == 'none') {
              let allUls = this.parentNode.parentNode.querySelectorAll('.nav__ul');
              let needCloseUls = [...allUls].filter(elem => {
                return (getComputedStyle(elem).getPropertyValue('display') != 'none' && elem != ul);
              });
              needCloseUls.forEach(elem => Drupal.wt.menu[toggleFunction](elem));
            }
            Drupal.wt.menu[toggleFunction](ul);
          }
          event.preventDefault();
          event.stopPropagation();
        });
      });
    }
  }

  Drupal.wt.scrollTo = Drupal.wt.scrollTo || function (uri) {
    if ((uri.indexOf(location.pathname) === 0 || uri.indexOf('#scroll') === 0) && uri.indexOf('#scroll') !== -1) {
      let selector = decodeURIComponent(uri.substring(uri.indexOf('#scroll') + '#scroll'.length));
      let regex = /^=[#\.\[\]a-zA-Z0-9-_]+$/;
      if (selector.search(regex) != -1) {
        selector = selector.substring(1);
        let element = document.querySelector(selector);
        try {
          let fixedOffsetWithSal = Drupal.wt.getFixedTop();
          let sal = element.getAttribute('data-sal');
          if (sal && element.classList.contains('sal-animate')) {
            if (sal.indexOf('-down')) {
              fixedOffsetWithSal += 100;
            }
            else if (sal.indexOf('-up')) {
              fixedOffsetWithSal -= 100;
            }
          }
          let elementFromTop = Math.floor(element.getBoundingClientRect().top);
          window.scrollBy({
            top: elementFromTop - fixedOffsetWithSal,
            left: 0,
            behavior: 'smooth'
          });
          dataLayer.push({
            'event': 'GAEvent',
            'eventCategory': 'scrollto',
            'eventAction': selector,
            'eventLabel': undefined,
            'eventNonInteraction': true
          });
        }
        catch (err) { /* fail silently */
        }
      }
      return false;
    }
  }

  Drupal.behaviors.wtScrollto = Drupal.behaviors.wtScrollto || {
    attach: function (context, settings) {
      $('a[href*="#scroll"]', context).once('wtScrollto').each(function () {
        $(this).on('click', function (event) {
          Drupal.wt.scrollTo($(this).attr('href'));
        });
      });
    }
  }

  Drupal.behaviors.wtImagesLoaded = Drupal.behaviors.wtImagesLoaded || {
    attach: function (context, settings) {
      $(document, context).once('wtImagesLoaded').each(function () {
        imagesLoaded(document.querySelector('body'), function () {
          window.setTimeout(Drupal.wt.scrollTo(location.pathname + location.hash), 200);
        })
      });
    }
  }

  Drupal.wt.findGALabel = Drupal.wt.findGALabel || function ($elem) {
    if ($elem.attr('data-ga-label')) {
      return $elem.attr('data-ga-label');
    }
    else if ($elem.parents('.wt__main').length > 0) {
      return 'main';
    }
    else if ($elem.parents('.wt__header').length > 0) {
      return 'header';
    }
    else if ($elem.parents('.wt__footer').length > 0) {
      return 'footer';
    }
    else if ($elem.parents('.wt__aside').length > 0) {
      return 'aside';
    }
    else {
      return undefined;
    }
  }

  Drupal.behaviors.wtAddGAEvents = Drupal.behaviors.wtAddGAEvents || {
    attach: function (context, settings) {
      $('a[href^="http://"], a[href^="https://"]', context).once('wtAddGAEventsOutbound').on('click', function () {
        try {
          let href = $(this).attr('href');
          if (RegExp('^https?://').test(href) && !RegExp(location.host).test(href) && href.indexOf('whatsapp.com/send') == -1 && !RegExp('(\.pdf|\.zip|\.docx?\.xlsx?)$', 'i').test(href)) {
            dataLayer.push({
              'event': 'GAEvent',
              'eventCategory': $(this).attr('data-ga-category') ? $(this).attr('data-ga-category') : 'outbound',
              'eventAction': href,
              'eventLabel': Drupal.wt.findGALabel($(this)),
              'eventNonInteraction': false
            });
          }
        }
        catch (err) { /* fail silently */
        }
      });
      $('a[href$=".pdf"], a[href$=".zip"], a[href$=".doc"], a[href$=".docx"], a[href$=".xls"], a[href$=".xlsx"]', context).once('wtAddGAEventsDownload').on('click', function () {
        try {
          dataLayer.push({
            'event': 'GAEvent',
            'eventCategory': $(this).attr('data-ga-category') ? $(this).attr('data-ga-category') : 'download',
            'eventAction': $(this).attr('href'),
            'eventLabel': Drupal.wt.findGALabel($(this)),
            'eventNonInteraction': false
          });
        }
        catch (err) { /* fail silently */
        }
      });
      $('a[href^="tel:"]', context).once('wtAddGAEventsPhone').on('click', 'a[href^="tel:"]', function () {
        try {
          dataLayer.push({
            'event': 'GAEvent',
            'eventCategory': $(this).attr('data-ga-category') ? $(this).attr('data-ga-category') : 'phone',
            'eventAction': $(this).attr('href').substring('tel:'.length),
            'eventLabel': Drupal.wt.findGALabel($(this)),
            'eventNonInteraction': false
          });
        }
        catch (err) { /* fail silently */
        }
      });
      $('a[href^="mailto:"]', context).once('wtAddGAEventsEmail').on('click', function () {
        try {
          dataLayer.push({
            'event': 'GAEvent',
            'eventCategory': $(this).attr('data-ga-category') ? $(this).attr('data-ga-category') : 'email',
            'eventAction': $(this).attr('href').substring('mailto:'.length),
            'eventLabel': Drupal.wt.findGALabel($(this)),
            'eventNonInteraction': false
          });
        }
        catch (err) { /* fail silently */
        }
      });
    }
  }


}(jQuery, Drupal, drupalSettings));
