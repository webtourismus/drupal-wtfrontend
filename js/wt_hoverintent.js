/**
 * @file
 * Webtourismus Frontend behaviors.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.wtOpenMenuOnHover = Drupal.behaviors.wtOpenMenuOnHover || {
    attach: function (context, settings) {
      $('.nav__a', context).once('wtOpenMenuOnHover').each(function () {
        let ul = this.nextElementSibling;
        if (ul && ul.tagName == 'UL') {
          hoverintent(
            ul.parentNode,
            function () {
              let computedStyles = getComputedStyle(ul);
              let hover = (computedStyles.getPropertyValue('--menu--open-on-hover') || 'false').trim() == 'true';
              let displayNone = computedStyles.getPropertyValue('display') == 'none';
              let animation = (computedStyles.getPropertyValue('--menu--animation') || 'collapse').trim();
              if (hover && displayNone) {
                drupalSettings.wt.menu[animation](ul);
              }
            },
            function () {
              let computedStyles = getComputedStyle(ul);
              let hover = (computedStyles.getPropertyValue('--menu--open-on-hover') || 'false').trim() == 'true';
              let displayNone = computedStyles.getPropertyValue('display') == 'none';
              let animation = (computedStyles.getPropertyValue('--menu--animation') || 'collapse').trim();
              if (hover && !displayNone) {
                drupalSettings.wt.menu[animation](ul);
              }
            }
          );
        }
      });
    }
  }

} (jQuery, Drupal, drupalSettings));
