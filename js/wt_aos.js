/**
 * @file
 * Webtourismus Frontend behaviors.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';
  drupalSettings.wt = drupalSettings.wt || {};
  drupalSettings.wt.aos = drupalSettings.wt.aos || {};

  Drupal.behaviors.wt_aos = Drupal.behaviors.wt_aos || {
    attach: function (context, settings) {
      $(document, context).once('wt_aos.init').each( function() {
        AOS.init(drupalSettings.wt.aos);
        $('[data-aos*="-left"], [data-aos*="-right"]').once('wt_aos.preventHorizontalScrollbar').each( function () {
          $(this).parents('.section').addClass('u-overflowX--hidden');
        });
      });
    }
  }
} (jQuery, Drupal, drupalSettings));
